// @flow
import React from "react";
import { StackNavigator, DrawerNavigator } from "react-navigation";
import { Root } from "native-base";

import Tax from "./screens/Tax/";
import Agro from "./screens/Agro";
import Sidebar from "./screens/Sidebar";
import CardView from "./components/cardView";
import CardAnalyticView from "./components/cardAnalyticView";
import PdfViewer from "./components/pdfViewer";
import Search from "./components/search";

const Drawer = DrawerNavigator(
  {
    Tax: { screen: Tax },
    Agro: { screen: Agro },
  },
  {
    initialRouteName: "Tax",
    contentComponent: props => <Sidebar {...props} />,
  }
);

const App = StackNavigator(
  {
    Drawer: { screen: Drawer },
    CardView: {
      screen: CardView,
      navigationOptions: {
        header: ({ goBack }) => ({
          left: <Left onPress={goBack} />,
        }),
      },
    },
    CardAnalyticView: {
      screen: CardAnalyticView,
      navigationOptions: {
        header: ({ goBack }) => ({
          left: <Left onPress={goBack} />,
        }),
      },
    },
    PdfViewer: {
      screen: PdfViewer,
      navigationOptions: {
        header: ({ goBack }) => ({
          left: <Left onPress={goBack} />,
        }),
      },
    },
    Search: { screen: Search },
  },
  {
    index: 0,
    initialRouteName: "Drawer",
    headerMode: "none",
  }
);

export default () => (
  <Root>
    <App />
  </Root>
);

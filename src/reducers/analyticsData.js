import type { Action } from "../actions/types";
import {
  GET_ANALYTICS_DATA_SUCCESS,
  GET_ANALYTICS_DATA_FAIL,
  SET_ANALYTICS_PAGE,
  IS_ANALYTICS_FETCHING,
  NO_ANALYTICS_NEW_DATA,
} from "../actions/analyticsData";
import { concat } from "lodash";

export type State = {
  data: Array,
  err: string,
};

const initialState = {
  data: null,
  err: null,
  page: 0,
  fetching: false,
  noNewData: false,
};

export default function(state: State = initialState, action: Action): State {
  if (action.type === GET_ANALYTICS_DATA_SUCCESS) {
    let d = state.data ? concat(state.data, action.data) : action.data;
    if (state.page === 0) {
      d = action.data;
    }
    return {
      ...state,
      data: d,
      err: null,
      fetching: false,
      noNewData: false,
    };
  }

  if (action.type === GET_ANALYTICS_DATA_FAIL) {
    return {
      ...state,
      data: null,
      err: action.err,
      fetching: false,
    };
  }

  if (action.type === SET_ANALYTICS_PAGE) {
    return {
      ...state,
      page: action.page || 0,
    };
  }

  if (action.type === IS_ANALYTICS_FETCHING) {
    return {
      ...state,
      fetching: true,
    };
  }

  if (action.type === NO_ANALYTICS_NEW_DATA) {
    return {
      ...state,
      noNewData: true,
    };
  }

  return state;
}

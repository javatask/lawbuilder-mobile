import { get } from 'lodash';

import React from "react";
import { View } from "react-native";
import { connect } from "react-redux";
import {
  Container,
  Header,
  Content,
  Text,
  Button,
  Icon,
  Card,
  CardItem,
  Spinner,
  Body
} from "native-base";

import * as analitics from "../../actions/analyticSingleData";

import styles from "./styles";

class CardAnalyticView extends React.PureComponent {
  // eslint-disable-line
  constructor(props) {
    super(props);
    this.state = {
      button: true
    };
    this.onPress = this.onPress.bind(this);
  }
  componentWillMount() {
    this.props.fetchDataById(this.props.navigation.state.params.nid);
  }

  onPress() {
    this.props.navigation.navigate("PdfViewer", {
      nid: get(this.props, 'databyid[0].nid'),
      url: get(this.props, 'databyid[0].field_analytics_file'),
    });
  }

  render() {
    // eslint-disable-line class-methods-use-this
    let content = (
      <Spinner
        color="green"
        style={{
          flex: 1,
          alignItems: "center",
          justifyContent: "center"
        }}
      />
    );
    if (this.props.databyid) {
      const {
        title,
        body,
        field_analytics_file,
        field_summary
      } = this.props.databyid[0];

      content = (
        <Container>
          <Header>
            <View style={styles.header}>
              <View style={styles.rowHeader}>
                <Button
                  transparent
                  style={styles.btnHeader}
                  onPress={() => this.props.navigation.goBack()}
                >
                  <Icon
                    name="ios-arrow-back-outline"
                    style={{ lineHeight: 30 }}
                  />
                </Button>
              </View>
            </View>
          </Header>

          <Content>
            <Card header style={styles.cardViewStyle}>
              <CardItem style={styles.cardViewHeader}>
                <Icon
                  style={styles.numberStyle}
                  active
                  name="ios-copy-outline"
                />
                <Text style={styles.textStyle}>{title}</Text>
              </CardItem>

              <CardItem>
                <Body>
                  <Text style={styles.textStyle}>{body}</Text>
                  <Text style={styles.authorTitle}>
                    Пропозиції громадськості:
                  </Text>

                  <Text style={styles.textStyle}>{field_summary}</Text>
                </Body>
              </CardItem>

              <CardItem>
                <View
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center"
                  }}
                >
                  <View style={{ flex: 1, flexDirection: "row" }}>
                    <Icon
                      style={styles.numberStyle}
                      active
                      name="ios-link-outline"
                    />
                    <Text
                      style={styles.textStyle}
                      onPress={this.onPress}
                    >
                      Продивитися файл
                    </Text>
                  </View>
                </View>
              </CardItem>
            </Card>
          </Content>
        </Container>
      );
    }

    return <View style={{ flex: 1 }}>{content}</View>;
  }
}
function bindAction(dispatch) {
  return {
    fetchDataById: nid => dispatch(analitics.getData(nid))
  };
}

const mapStateToProps = state => ({
  databyid: state.analyticSingleData.data
});
export default connect(mapStateToProps, bindAction)(CardAnalyticView);

import React from "react";
import {
  Text,
  Icon,
  Card,
  CardItem,
  Badge,
  Body,
  ListItem,
} from "native-base";

import { concat } from "lodash";
import styles from "./styles";

class CardRadaItem extends React.PureComponent {
  // eslint-disable-line

  constructor(props) {
    super(props);
    this.onPress = this.onPress.bind(this);
  }
  onPress() {
    this.props.navigation.navigate("CardView", { nid: this.props.el.nid });
  }
  render() {
    // eslint-disable-line class-methods-use-this
    const {
      title,
      field_date_decision,
      field_full_name,
      field_zakon_avtor,
      field_initiator_official,
      field_summary,
      field_documents,
    } = this.props.el;

    // Case from views or search api
    let authors = field_zakon_avtor || [];
    if (typeof field_zakon_avtor === "string") {
      authors = field_zakon_avtor.split(",");
    }
    let auth = field_initiator_official || [];
    if (typeof field_initiator_official === "string") {
      auth = field_initiator_official.split(",");
    }

    authors = concat(authors, auth);

    const lineSize = Math.round(field_full_name.length / 30) + 4 || 1;
    let statusIcon = null;
    switch (field_summary) {
      case "Так":
        statusIcon = (
          <Icon name="ios-thumbs-up-outline" style={styles.likeIcon} />
        );
        break;
      case "Ні":
        statusIcon = (
          <Icon name="ios-thumbs-down-outline" style={styles.likeIcon} />
        );
        break;
      default:
        statusIcon = <Icon name="ios-timer-outline" style={styles.likeIcon} />;
    }

    return (
      <ListItem>
        <Card foregroundColor="#222" >
          <CardItem
            style={styles.cardHeader}
            header
            button
            onPress={() => this.onPress()}
          >
            <Icon style={styles.iconCard} name="ios-attach-outline" />
            <Text style={styles.cmtName}>{field_date_decision}</Text>
            <Text style={styles.cmtName}>{title}</Text>
          </CardItem>

          <CardItem button onPress={() => this.onPress()}>
            <Body>
              <Text numberOfLines={lineSize} style={styles.footerName}>
                {field_full_name}
              </Text>
            </Body>
          </CardItem>

          <CardItem
            style={styles.cardFooter}
            footer
            button
            onPress={() => this.onPress()}
          >
            <Icon
              name="ios-people"
              style={{
                fontSize: 32,
                color: "#606060",
              }}
            />
            <Text style={styles.footerName}>
              {authors[0] ? authors[0] : null}
            </Text>
            <Badge info>
              <Text>{authors.length || 0}</Text>
            </Badge>
          </CardItem>
        </Card>
      </ListItem>
    );
  }
}

export default CardRadaItem;

import type { Action } from "./types";
import siteUrl from "../boot/config";

export const GET_ANALYTICS_DATA = "GET_ANALYTICS_DATA";
export const GET_ANALYTICS_DATA_SUCCESS = "GET_ANALYTICS_DATA_SUCCESS";
export const GET_ANALYTICS_DATA_FAIL = "GET_ANALYTICS_DATA_FAIL";
export const SET_ANALYTICS_PAGE = "SET_ANALYTICS_PAGE";
export const IS_ANALYTICS_FETCHING = "IS_ANALYTICS_FETCHING";
export const NO_ANALYTICS_NEW_DATA = "NO_ANALYTICS_NEW_DATA";

export function getDataSuccess(data) {
  return { type: GET_ANALYTICS_DATA_SUCCESS, data };
}

export function getDataFail(err) {
  return { type: GET_ANALYTICS_DATA_FAIL, err };
}

export function isFetching() {
  return { type: IS_ANALYTICS_FETCHING };
}

export function noNewData() {
  return { type: NO_ANALYTICS_NEW_DATA };
}

export function setPage(page) {
  return { type: SET_ANALYTICS_PAGE, page };
}

export function getData(page) {
  return (dispatch, getState) => {
    if (getState().analyticsData.fetching) {
      return;
    }
    dispatch(setPage(page));
    dispatch(isFetching());
    fetch(`${siteUrl}/rest/analytics-rest-api?page=${page}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then(response => response.json())
      .then(values => {
        if (values.length === 0) {
          dispatch(noNewData(values));
        } else {
          dispatch(getDataSuccess(values));
        }
      })
      .catch(err => dispatch(getDataFail(err)));
  };
}

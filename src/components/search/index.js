import React, { Component } from "react";
import {
  View,
  FlatList,
} from "react-native";
import { connect } from "react-redux";
import {
  Container,
  Header,
  Content,
  Text,
  Button,
  Icon,
  InputGroup,
  Input,
  Spinner,
} from "native-base";

import { getSearchData } from "../../actions/search";
import CardRadaItem from "../cardRadaItem";

import styles from "./styles";

class Search extends Component {
  // eslint-disable-line

  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
    this.onSearch = this.onSearch.bind(this);
  }
  componentWillReceiveProps(nextprops) {
    nextprops.searchFetching
      ? null
      : this.setState({ data: nextprops.searchData });
  }
  _keyExtractor = (item, index) => index;
  _renderItem = ({ item }) => {
    return <CardRadaItem el={item} navigation={this.props.navigation} />;
  };
  onSearch = event => {
    this.props.searchThunk(event.nativeEvent.text);
  };

  // TODO Place in single place
  render() {
    // eslint-disable-line class-methods-use-this
    let searchContent = null;
    if (this.props.searchFetching) {
      searchContent = (
        <Spinner
          color="green"
          style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
          }}
        />
      );
    } else if (this.props.searchData) {
      searchContent = (
        <FlatList
          data={this.state.data}
          keyExtractor={this._keyExtractor}
          renderItem={this._renderItem}
        />
      );
    } else if (this.props.err) {
      searchContent = (
        <View style={styles.errMsg}>
          <Button transparent onPress={this.getNewData}>
            <View
              style={{
                flex: 1,
                flexDirection: "column",
                justifyContent: "space-between",
                alignSelf: "stretch",
              }}
            >
              <Icon
                name="ios-thunderstorm-outline"
                style={{
                  lineHeight: 30,
                  fontSize: 32,
                }}
              />
              <Text>Щось пішло не так, спробувати ще раз</Text>
            </View>
          </Button>
        </View>
      );
    } else if (this.props.searchNoNewData) {
      searchContent = (
        <View style={styles.errMsg}>
          <View
            style={{
              flex: 1,
              flexDirection: "column",
              justifyContent: "space-between",
              alignSelf: "stretch",
            }}
          >
            <Icon
              name="ios-alert-outline"
              style={{
                lineHeight: 30,
                fontSize: 32,
              }}
            />
            <Text>Дані за запитом - відсутні</Text>
          </View>
        </View>
      );
    }

    return (
      <Container>
        <Header search rounded>
          <View style={styles.header}>
            <View style={styles.rowHeader}>
              <Button
                transparent
                style={styles.btnHeader}
                onPress={() => this.props.navigation.goBack()}
              >
                <Icon
                  name="ios-arrow-back-outline"
                  style={{
                    lineHeight: 30,
                  }}
                />
              </Button>
              <InputGroup style={styles.searchHeader}>
                <Icon name="ios-search" />
                <Input placeholder="Пошук" onSubmitEditing={this.onSearch} />
              </InputGroup>
            </View>
          </View>
        </Header>

        <Content padder>{searchContent}</Content>
      </Container>
    );
  }
}

function bindAction(dispatch) {
  return {
    searchThunk: text => dispatch(getSearchData(text)),
  };
}

const mapStateToProps = state => ({
  searchData: state.search.searchData,
  err: state.search.searchErr,
  searchFetching: state.search.searchFetching,
  searchNoNewData: state.search.searchNoNewData,
  searchText: state.search.searchText,
});

export default connect(mapStateToProps, bindAction)(Search);

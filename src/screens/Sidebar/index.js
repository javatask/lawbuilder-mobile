// @flow
import React, { Component } from "react";

import { Container, Content, Text, Icon, ListItem } from "native-base";

import styles from "./style";

class SideBar extends Component {
  render() {
    const navigation = this.props.navigation;
    return (
      <Container>
        <Content style={styles.drawerContent}>
          <ListItem iconLeft style={styles.links}>
            <Text style={styles.linkMainText}>Законотворчість</Text>
          </ListItem>

          <ListItem
            button
            onPress={() => {
              navigation.navigate("Tax");
            }}
            iconLeft
            style={styles.links}
          >
            <Icon name="ios-card-outline" />
            <Text style={styles.linkText}>Податковий</Text>
          </ListItem>
          <ListItem
            button
            onPress={() => {
              navigation.navigate("Agro");
            }}
            iconLeft
            style={styles.links}
          >
            <Icon name="ios-leaf-outline" />
            <Text style={styles.linkText}>Аграрний</Text>
          </ListItem>
        </Content>
      </Container>
    );
  }
}

export default SideBar;

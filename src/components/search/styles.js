const React = require("react-native");

var { Dimensions, Platform } = React;
//var StyleSheet = require('react-native-debug-stylesheet');

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
  container: {
    flex: 1,
    width: null,
    height: null,
  },
  logoHeader: {
    width: 20,
    height: 28,
    alignSelf: "center",
  },
  text: {
    fontSize: 15,
    color: "white",
    marginBottom: 10,
  },
  header: {
    width: Dimensions.get("window").width,
    flexDirection: "row",
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: Platform.OS === "ios" ? 30 : undefined,
    marginLeft: Platform.OS === "ios" ? undefined : -30,
  },
  rowHeader: {
    flex: 1,
    flexDirection: "row",
    //justifyContent: 'space-between',
    alignSelf: "stretch",
    paddingTop: Platform.OS === "android" ? 7 : 0,
  },
  backHeader: {
    flex: 0.2,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingTop: Platform.OS === "android" ? 7 : 0,
  },
  searchHeader: {
    flex: 1,
    flexDirection: "row",
    alignSelf: "stretch",
    paddingTop: Platform.OS === "android" ? 7 : 0,
  },
  btnHeader: {
    paddingTop: 10,
  },
  imageHeader: {
    height: 25,
    width: 95,
    resizeMode: "contain",
    marginTop: 5,
  },
  helpBtns: {
    opacity: 0.9,
    fontSize: 14,
    color: "black",
    fontWeight: "bold",
  },
  errMsg: {
    flex: 1,
    width: deviceHeight < 500 ? 160 : deviceWidth / 4 + 24,
    height: deviceHeight < 500 ? 100 : deviceHeight / 30,
    alignSelf: "center",
    marginTop: deviceWidth < 330 ? deviceHeight / 15 : deviceHeight / 6,
  },
};

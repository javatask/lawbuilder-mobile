import type { Action } from "./types";
import siteUrl from "../boot/config";

export const GET_ANALYTIC_DATA = "GET_ANALYTIC_DATA";
export const GET_ANALYTIC_DATA_SUCCESS = "GET_ANALYTIC_DATA_SUCCESS";
export const GET_ANALYTIC_DATA_FAIL = "GET_ANALYTIC_DATA_FAIL";
export const IS_ANALYTIC_FETCHING = "IS_ANALYTIC_FETCHING";

export function getDataSuccess(data): Action {
  return { type: GET_ANALYTIC_DATA_SUCCESS, data };
}

export function getDataFail(err): Action {
  return { type: GET_ANALYTIC_DATA_FAIL, err };
}

export function isFetching(): Action {
  return { type: IS_ANALYTIC_FETCHING };
}

export function getData(nid) {
  return (dispatch, getState) => {
    if (getState().analyticSingleData.fetching) {
      return;
    }
    dispatch(isFetching());
    fetch(`${siteUrl}/rest/analytics_endpoint?args[0]=${nid}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then(response => response.json())
      .then(values => {
        dispatch(getDataSuccess(values));
      })
      .catch(err => dispatch(getDataFail(err)));
  };
}

import { get } from "lodash";
import React, { Component } from "react";
import { View, StyleSheet, WebView, Platform, Dimensions } from "react-native";
import { Constants } from "expo";

import {
  Container,
  Header,
  Content,
  Text,
  Button,
  Icon,
} from "native-base";

import siteUrl from "../../boot/config";
import styles from "./styles";

export default class PdfViewer extends Component {
  render() {
    const url = get(this.props, "navigation.state.params.url");
    const nid = get(this.props, "navigation.state.params.nid");
    let viewer = <Text>Файл недоступний</Text>;
    if (url && Platform.OS === "ios") {
      viewer = (
        <WebView
          style={{width: Dimensions.get('window').width, height: Dimensions.get('window').height }}
          bounces={false}
          scrollEnabled={false}
          source={{ uri: url }} />
      );
    }
    if (url && Platform.OS === "android") {
      viewer = (
        <WebView
          style={{width: Dimensions.get('window').width, height: Dimensions.get('window').height*2}}
          bounces={false}
          scrollEnabled={true}
          allowUniversalAccessFromFileURLs={true}
          source={{ uri: `${siteUrl}/node/${nid}` }}
        />
      );
    }
    return (
      <Container>
        <Header>
          <View style={styles.header}>
            <View style={styles.rowHeader}>
              <Button
                transparent
                style={styles.btnHeader}
                onPress={() => this.props.navigation.goBack()}
              >
                <Icon
                  name="ios-arrow-back-outline"
                  style={{ lineHeight: 30 }}
                />
              </Button>
            </View>
          </View>
        </Header>

        <Content style={{paddingTop: 20}}>
          {viewer}
        </Content>
      </Container>
    );
  }
}
//
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     paddingTop: Constants.statusBarHeight,
//     height: "100%"
//   }
// });

import type { Action } from "../actions/types";
import {
  GET_DATA_SUCCESS,
  GET_DATA_FAIL,
  SET_PAGE,
  IS_FETCHING,
  NO_NEW_DATA,
  SET_COMMITTEE_FILTER,
  DELETE_COMMITTEE_FILTER,
} from "../actions/data";
import { concat, uniq } from "lodash";

export type State = {
  data: Array,
  committeeFilter: string,
  err: string,
};

const initialState = {
  data: null,
  committeeFilter: [72, 90], // 72 - tax, 90 - agro
  err: null,
  page: 0,
  fetching: false,
  noNewData: false,
};

export default function(state: State = initialState, action: Action): State {
  if (action.type === GET_DATA_SUCCESS) {
    let d = state.data ? concat(state.data, action.data) : action.data;
    if (state.page === 0) {
      d = action.data;
    }
    return {
      ...state,
      data: d,
      err: null,
      fetching: false,
      noNewData: false,
    };
  }

  if (action.type === GET_DATA_FAIL) {
    return {
      ...state,
      data: null,
      err: action.err,
      fetching: false,
    };
  }

  if (action.type === SET_PAGE) {
    return {
      ...state,
      page: action.page || 0,
    };
  }

  if (action.type === IS_FETCHING) {
    return {
      ...state,
      fetching: true,
    };
  }

  if (action.type === NO_NEW_DATA) {
    return {
      ...state,
      noNewData: true,
    };
  }

  if (action.type === SET_COMMITTEE_FILTER) {
    return {
      ...state,
      committeeFilter: uniq([...state.committeeFilter, action.filter]),
    };
  }

  if (action.type === DELETE_COMMITTEE_FILTER) {
    return {
      ...state,
      committeeFilter: state.committeeFilter.filter(el => el != action.filter),
    };
  }

  return state;
}

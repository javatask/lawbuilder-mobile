import { combineReducers } from "redux";

import data from "./data";
import analyticsData from "./analyticsData";
import analyticSingleData from "./analyticSingleData";
import lawSingleData from "./lawSingleData";
import search from "./search";

export default combineReducers({
  data,
  analyticsData,
  analyticSingleData,
  lawSingleData,
  search,
});

// @flow
import React, { Component } from "react";
import { Image } from "react-native";
import { connect } from "react-redux";
import {
  Container,
  Header,
  Text,
  Button,
  Icon,
  Left,
  Body,
  Right,
  Spinner,
  Tabs,
  Tab,
} from "native-base";

import * as law from "../../actions/data";
import * as analitics from "../../actions/analyticsData";
import CardList from "../../components/cardList";
import styles from "./styles";
import CardAnalyticsList from "../../components/cardAnalyticsList";
const headerLogo = require("../../../assets/header-logo.png");

class Tax extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    if (this.props.isLoading) {
      return <Spinner />;
    } else {
      return (
        <Container>
          <Header hasTabs>
            <Left>
              <Button
                transparent
                onPress={() => this.props.navigation.navigate("DrawerOpen")}
              >
                <Icon active name="menu" />
              </Button>
            </Left>
            <Body>
              <Image source={headerLogo} style={styles.imageHeader} />
              <Text>Податковий</Text>
            </Body>
            <Right>
              <Button
                transparent
                onPress={() => this.props.navigation.navigate("Search")}
              >
                <Icon name="ios-search" />
              </Button>
            </Right>
          </Header>
          <Tabs style={{ backgroundColor: "#fff" }} initialPage={0}>
            <Tab heading="Законопроекти">
              <CardList
                data={this.props.data}
                fetching={this.props.fetching}
                fetchdata={this.props.fetchData}
                navigation={this.props.navigation}
                committe="72"
              />
            </Tab>
            <Tab heading="Аналітика">
              <CardAnalyticsList
                data={this.props.dataAnalitics}
                fetching={this.props.fetchingAnalitics}
                fetchdata={this.props.fetchAnaliticsData}
                navigation={this.props.navigation}
              />
            </Tab>
          </Tabs>
        </Container>
      );
    }
  }
}

function bindAction(dispatch) {
  return {
    fetchData: (page, committe) => dispatch(law.getData(page, committe)),
    fetchAnaliticsData: page => dispatch(analitics.getData(page)),
  };
}

const mapStateToProps = state => ({
  data: state.data.data,
  fetching: state.data.fetching,
  dataAnalitics: state.analyticsData.data,
  fetchingAnalitics: state.analyticsData.fetching,
});
export default connect(mapStateToProps, bindAction)(Tax);

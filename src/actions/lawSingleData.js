import type { Action } from "./types";
import siteUrl from "../boot/config";

export const GET_LAW_DATA = "GET_LAW_DATA";
export const GET_LAW_DATA_SUCCESS = "GET_LAW_DATA_SUCCESS";
export const GET_LAW_DATA_FAIL = "GET_LAW_DATA_FAIL";
export const IS_LAW_FETCHING = "IS_LAW_FETCHING";

export function getDataSuccess(data): Action {
  return { type: GET_LAW_DATA_SUCCESS, data };
}

export function getDataFail(err): Action {
  return { type: GET_LAW_DATA_FAIL, err };
}

export function isFetching(): Action {
  return { type: IS_LAW_FETCHING };
}

export function getData(nid) {
  return (dispatch, getState) => {
    if (getState().lawSingleData.fetching) {
      return;
    }
    dispatch(isFetching());
    fetch(`${siteUrl}/rest/law_endpoint?args[0]=${nid}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then(response => response.json())
      .then(values => {
        dispatch(getDataSuccess(values));
      })
      .catch(err => dispatch(getDataFail(err)));
  };
}

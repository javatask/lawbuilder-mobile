import type { Action } from "../actions/types";
import {
  GET_ANALYTIC_DATA_SUCCESS,
  GET_ANALYTIC_DATA_FAIL,
  IS_ANALYTIC_FETCHING,
} from "../actions/analyticSingleData";

export type State = {
  data: Array,
  err: string,
};

const initialState = {
  data: null,
  err: null,
  fetching: false,
};

export default function(state: State = initialState, action: Action): State {
  if (action.type === GET_ANALYTIC_DATA_SUCCESS) {
    return {
      ...state,
      data: action.data,
      err: null,
      fetching: false,
      noNewData: false,
    };
  }

  if (action.type === GET_ANALYTIC_DATA_FAIL) {
    return {
      ...state,
      data: null,
      err: action.err,
      fetching: false,
    };
  }

  if (action.type === IS_ANALYTIC_FETCHING) {
    return {
      ...state,
      fetching: true,
    };
  }

  return state;
}

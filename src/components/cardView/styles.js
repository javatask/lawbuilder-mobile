const React = require("react-native");

const { Dimensions, Platform } = React;
console.log(Dimensions.get("window").width);
export default {
  container: {
    flex: 1,
    width: null,
    height: null,
  },
  webViewStyle: {
    flex: 1,
    margin: 20,
  },
  logoHeader: {
    width: 20,
    height: 28,
    alignSelf: "center",
  },
  text: {
    fontSize: 15,
    color: "#000",
    marginBottom: 10,
  },
  header: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-start",
    paddingLeft: 15,
    paddingRight: 15,
    marginLeft: Platform.OS === "ios" ? undefined : -30,
  },
  rowHeader: {
    alignSelf: "stretch",
    paddingTop: Platform.OS === "android" ? 7 : 0,
  },
  btnHeader: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingTop: 10,
  },
  imageHeader: {
    height: 25,
    width: 95,
    resizeMode: "contain",
    marginTop: 5,
  },
  helpBtns: {
    opacity: 0.9,
    fontSize: 14,
    color: "black",
    fontWeight: "bold",
  },

  /*--------------------avm styles--------------------*/

  //title text inside of cardview
  numberStyle: {
    fontWeight: "bold",
    color: "#1e90ff",
    textAlign: "center",
  },

  //regular text inside of cardview
  textStyle: {
    flex: 1,
    color: "#696969",
  },

  //card style inside of card view
  cardViewStyle: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#000",
    marginLeft: 20,
    marginRight: 20
  },

  //card view Header
  cardViewHeader: {
    flex: 1,
    backgroundColor: "#e6e6fa",
  },

  //authors title
  authorTitle: {
    flex: 1,
    color: "#696969",
    fontWeight: "bold",
  },
  // card body
  cardBody: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
    alignSelf: "stretch",
  },
};

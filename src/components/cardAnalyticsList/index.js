import React from "react";
import {
  FlatList,
} from "react-native";

import {
  Spinner,
  View,
} from "native-base";

import CardAnalyticsItem from "./../cardAnalyticsItem";

class CardAnalyticsList extends React.PureComponent {
  // eslint-disable-line

  constructor(props) {
    super(props);
    this.state = {
      isFetching: this.props.fetching,
      data: [],
      page: 0,
    };
    this.onRefresh = this.onRefresh.bind(this);
    this.onEndReached = this.onEndReached.bind(this);
  }
  componentDidMount() {
    this.state = {
      isFetching: this.props.fetching,
      data: [],
      page: 0,
    };
    this.props.fetchdata(this.state.page);
  }

  _keyExtractor = (item, index) => index.toString();
  _renderItem = ({ item }) => {
    return <CardAnalyticsItem key={item.nid.toString()} el={item} navigation={this.props.navigation} />;
  };
  componentWillReceiveProps(nextprops) {
    nextprops.fetching
      ? null
      : this.setState({
          isFetching: false,
          data: this.state.data.concat(nextprops.data),
        });
  }
  onRefresh() {
    this.setState({ isFetching: true, data: [], page: 0 });
    this.props.fetchdata(0);
  }
  onEndReached(toEnd) {
    this.setState({
      page: this.state.page + 1,
    });
    this.props.fetchdata(this.state.page);
  }
  render() {
    // eslint-disable-line class-methods-use-this
    let content = (
      <Spinner
        color="green"
        style={{
          flex: 1,
          alignItems: "center",
          justifyContent: "center",
        }}
      />
    );

    if (this.props.data) {
      content = (
        <FlatList
          refreshing={this.state.isFetching}
          scrollEventThrottle={200}
          data={this.state.data}
          keyExtractor={this._keyExtractor}
          renderItem={this._renderItem}
          onRefresh={this.onRefresh}
          onEndReached={this.onEndReached}
          onEndReachedThreshold={0.5}
        />
      );
    }

    return <View style={{ flex: 1 }}>{content}</View>;
  }
}

export default CardAnalyticsList;

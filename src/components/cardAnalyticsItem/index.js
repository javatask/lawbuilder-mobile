import React from "react";
import {
  Text,
  Icon,
  Card,
  CardItem,
  Body,
  ListItem,
} from "native-base";

import moment from "moment";
import styles from "./styles";

class CardAnalyticsItem extends React.PureComponent {
  // eslint-disable-line
  constructor(props) {
    super(props);
    this.onPress = this.onPress.bind(this);
  }
  onPress() {
    this.props.navigation.navigate("CardAnalyticView", {
      nid: this.props.el.nid,
    });
  }

  render() {
    // eslint-disable-line class-methods-use-this
    const { nid, title, changed, name, field_summary } = this.props.el;

    let statusIcon = <Icon name="ios-help" style={styles.iconCardHeader} />;
    switch (field_summary) {
      case "Відсутні":
        statusIcon = <Icon name="ios-help" style={styles.iconCardHeader} />;
        break;
      case "Підтримати":
        statusIcon = (
          <Icon name="ios-thumbs-up-outline" style={styles.iconCardHeader} />
        );
        break;
      case "Відхилити":
        statusIcon = (
          <Icon name="ios-thumbs-down-outline" style={styles.iconCardHeader} />
        );
        break;
      case "Доопрацювати":
        statusIcon = (
          <Icon name="ios-build-outline" style={styles.iconCardHeader} />
        );
        break;
      case "Так":
        statusIcon = (
          <Icon name="ios-thumbs-up-outline" style={styles.iconCardHeader} />
        );
        break;
      case "Ні":
        statusIcon = (
          <Icon name="ios-thumbs-down-outline" style={styles.iconCardHeader} />
        );
        break;
    }
    return (
      <ListItem key={nid.toString()}>
        <Card foregroundColor="#222">
          <CardItem
            style={styles.cardHeader}
            header
            button
            onPress={() => this.onPress()}
          >
            <Icon style={styles.iconCardHeader} name="ios-time-outline" />
            <Text style={styles.date}>
              {moment(changed, "DD.MM.YYYY").format("DD.MM.YYYY")}
            </Text>
            {statusIcon}
          </CardItem>

          <CardItem
            style={styles.cardBody}
            button
            onPress={() => this.onPress()}
          >
            <Body>
              <Text style={styles.bodyName}>{title}</Text>
            </Body>
          </CardItem>

          <CardItem
            style={styles.cardFooter}
            footer
            button
            onPress={() => this.onPress()}
          >
            <Icon style={styles.iconCardFooter} name="ios-person-outline" />
            <Text style={styles.footerName}>{name}</Text>
          </CardItem>
        </Card>
      </ListItem>
    );
  }
}

export default CardAnalyticsItem;

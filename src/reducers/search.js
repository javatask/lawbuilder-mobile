import type { Action } from "../actions/types";
import {
  GET_SEARCH_DATA_SUCCESS,
  GET_SEARCH_DATA_FAIL,
  IS_SEARCH_FETCHING,
  NO_NEW_SEARCH_DATA,
} from "../actions/search";

export type State = {
  searchData: Array,
  searchErr: string,
};

const initialState = {
  searchData: null,
  searchErr: null,
  searchFetching: false,
  searchNoNewData: false,
  searchText: "",
};

export default function(state: State = initialState, action: Action): State {
  if (action.type === GET_SEARCH_DATA_SUCCESS) {
    return {
      ...state,
      searchData: action.data,
      searchErr: null,
      searchFetching: false,
      searchNoNewData: false,
    };
  }

  if (action.type === GET_SEARCH_DATA_FAIL) {
    return {
      ...state,
      searchData: null,
      searchErr: action.err,
      searchFetching: false,
      searchText: "",
    };
  }

  if (action.type === IS_SEARCH_FETCHING) {
    return {
      ...state,
      searchFetching: true,
      searchText: action.text,
    };
  }

  if (action.type === NO_NEW_SEARCH_DATA) {
    return {
      ...state,
      searchNoNewData: true,
      searchFetching: false,
      searchData: null,
      searchText: "",
    };
  }

  return state;
}

import type { Action } from "./types";
import siteUrl from "../boot/config";

export const GET_SEARCH_DATA = "GET_SEARCH_DATA";
export const GET_SEARCH_DATA_SUCCESS = "GET_SEARCH_DATA_SUCCESS";
export const GET_SEARCH_DATA_FAIL = "GET_SEARCH_DATA_FAIL";
export const IS_SEARCH_FETCHING = "IS_SEARCH_FETCHING";
export const NO_NEW_SEARCH_DATA = "NO_NEW_SEARCH_DATA";

export function getSearchDataSuccess(data): Action {
  return { type: GET_SEARCH_DATA_SUCCESS, data };
}

export function getSearchDataFail(err): Action {
  return { type: GET_SEARCH_DATA_FAIL, err };
}

export function isSearchFetching(text): Action {
  return { type: IS_SEARCH_FETCHING, text };
}

export function noNewSearchData(): Action {
  return { type: NO_NEW_SEARCH_DATA };
}

export function getSearchData(text) {
  return (dispatch, getState) => {
    //const committie = getState().data.committeeFilter.reduce((e, el) => `${e},${el}`);
    if (getState().search.searchFetching) {
      return;
    }
    dispatch(isSearchFetching(text));
    fetch(`${siteUrl}/rest/ssapi?args[0]=${text}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then(response => response.json())
      .then(values => {
        if (values.length === 0) {
          dispatch(noNewSearchData(values));
        } else {
          dispatch(getSearchDataSuccess(values));
        }
      })
      .catch(err => dispatch(getSearchDataFail(err)));
  };
}

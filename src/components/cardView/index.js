import isEmpty from "lodash/isEmpty";
import concat from "lodash/concat";

import he from "he";

import React from "react";
import { View } from "react-native";
import {
  Container,
  Header,
  Content,
  Text,
  Button,
  Icon,
  Card,
  CardItem,
  Spinner,
  Body
} from "native-base";

import { connect } from "react-redux";
//import he from 'he'; import {web} from 'react-native-communications';

import { WebBrowser } from "expo";
import styles from "./styles";

import * as singledata from "../../actions/lawSingleData";

class CardView extends React.PureComponent {
  // eslint-disable-line
  static navigationOptions = {
    title: "CardView",
    header: null
  };
  constructor(props) {
    super(props);
    this.state = {
      result: null
    };
  }
  componentWillMount() {
    this.props.fetchDataById(this.props.navigation.state.params.nid);
  }
  _handlePressButtonAsync = async () => {
    let result = await WebBrowser.openBrowserAsync(
      this.props.databyid[0].field_rada_url
    );
    this.setState({ result });
  };
  render() {
    // eslint-disable-line class-methods-use-this
    let content = (
      <Spinner
        color="green"
        style={{
          flex: 1,
          alignItems: "center",
          justifyContent: "center"
        }}
      />
    );
    if (this.props.databyid) {
      const {
        title,
        field_full_name,
        field_zakon_avtor,
        field_initiator_official
      } = this.props.databyid[0];
      let authors = field_zakon_avtor || null;
      if (typeof field_zakon_avtor === "string") {
        authors = field_zakon_avtor.split(",");
      }

      let numOfLines = 1;
      let authorsStr = null;
      if (!isEmpty(authors)) {
        numOfLines += authors.length;
        authorsStr = authors.reduce((m, p) => `${m}\n${p}`, "");
      }

      let auth = field_initiator_official || null;
      if (typeof field_initiator_official === "string") {
        auth = field_initiator_official.split(",");
      }

      let authStr = null;
      if (!isEmpty(auth)) {
        numOfLines += auth.length;
        authStr = auth.reduce((m, p) => `${m}\n${p}`, "");
      }

      let allAuthors = "";
      if (!isEmpty(authStr)) {
        allAuthors += authStr;
      }

      if (!isEmpty(authorsStr)) {
        allAuthors += authorsStr;
      }

      console.log(allAuthors);

      content = (
        <Container>
          <Header >
            <View style={styles.header}>
            <Button
              transparent
              style={styles.btnHeader}
              onPress={() => this.props.navigation.goBack()}
            >
              <Icon
                name="ios-arrow-back-outline"
                style={{
                  lineHeight: 30
                }}
              />
            </Button>
            </View>
          </Header>

          <Content>
            <View style={styles.cardViewStyle}>
              <CardItem
                style={styles.cardViewHeader}
                header
                button
                onPress={() => this._handlePressButtonAsync()}
              >
                <Icon
                  style={styles.numberStyle}
                  active
                  name="ios-copy-outline"
                />
                <Text style={styles.numberStyle}>{title}</Text>
              </CardItem>

              <CardItem
                style={{
                  flex: 1,
                  flexDirection: "column",
                  justifyContent: "space-between",
                  alignItems: "center"
                }}
                button
                onPress={() => this._handlePressButtonAsync()}
              >
                <Text style={styles.textStyle}>
                  {he.decode(field_full_name)}
                </Text>
                <Text style={styles.authorTitle}>Автори:</Text>
                <Text numberOfLines={numOfLines} style={styles.textStyle}>
                  {allAuthors}
                </Text>
              </CardItem>

              <CardItem
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center"
                }}
                footer
                button
                onPress={() => this._handlePressButtonAsync()}
              >
                <Icon
                  style={styles.numberStyle}
                  active
                  name="ios-link-outline"
                />
                <Text style={styles.textStyle}>Детальніше</Text>
              </CardItem>
            </View>
          </Content>
        </Container>
      );
    }
    return <View style={{ flex: 1 }}>{content}</View>;
  }
}
function bindAction(dispatch) {
  return {
    fetchDataById: nid => dispatch(singledata.getData(nid))
  };
}

const mapStateToProps = state => ({
  databyid: state.lawSingleData.data
});
export default connect(mapStateToProps, bindAction)(CardView);

import type { Action } from "./types";
import siteUrl from "../boot/config";

export const GET_DATA = "GET_DATA";
export const GET_DATA_SUCCESS = "GET_DATA_SUCCESS";
export const GET_DATA_FAIL = "GET_DATA_FAIL";
export const SET_PAGE = "SET_PAGE";
export const IS_FETCHING = "IS_FETCHING";
export const NO_NEW_DATA = "NO_NEW_DATA";
// Filtering
export const SET_COMMITTEE_FILTER = "SET_COMMITTEE_FILTER";
export const DELETE_COMMITTEE_FILTER = "DELETE_COMMITTEE_FILTER";
// Navigation
export const ON_NAVIGATE = "ON_NAVIGATE";
export const OFF_NAVIGATE = "OFF_NAVIGATE";

export function onNaviaget(on) {
  return on ? { type: ON_NAVIGATE } : { type: OFF_NAVIGATE }; // tax 276  agro 302;
}

export function selectCommittee(include, filter) {
  return include
    ? { type: SET_COMMITTEE_FILTER, filter }
    : { type: DELETE_COMMITTEE_FILTER, filter }; // tax 276  agro 302;
}

export function updateFilter(include, filter) {
  return (dispatch, getState) => {
    dispatch(selectCommittee(include, filter));
    dispatch(getData(0, "90"));
  };
}

export function getDataSuccess(data) {
  return { type: GET_DATA_SUCCESS, data };
}

export function getDataFail(err) {
  return { type: GET_DATA_FAIL, err };
}

export function isFetching() {
  return { type: IS_FETCHING };
}

export function noNewData() {
  return { type: NO_NEW_DATA };
}

export function setPage(page) {
  return { type: SET_PAGE, page };
}

export function getData(page, committe) {
  return (dispatch, getState) => {
    if (getState().data.fetching) {
      return;
    }
    dispatch(setPage(page));
    dispatch(isFetching());
    fetch(`${siteUrl}/rest/latest?page=${page}&args[0]=${committe}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then(response => response.json())
      .then(values => {
        if (values.length === 0) {
          dispatch(noNewData(values));
        } else {
          dispatch(getDataSuccess(values));
        }
      })
      .catch(err => dispatch(getDataFail(err)));
  };
}

export function registerPush(token) {
  return (dispatch, getState) => {
    fetch(`${siteUrl}/rest/push_notifications`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        token: token.token,
        type: token.os,
      }),
    });
  };
}
